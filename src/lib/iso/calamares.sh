#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

#{{{ calamares

yaml_array() {
    local array yaml

    for entry in "$@"; do
        yaml="{name: ${entry}, action: enable, alias: add, target: default}"
        array="${array:-}${array:+,} ${yaml}"
    done
    printf "%s\n" "[${array}]"
}

configure_calamares(){
    local mods="$1/etc/calamares/modules"
    if [[ -d "$mods" ]];then
        msg2 "Configuring: Calamares"

        if [[ -f "$mods"/services-artix.conf ]]; then
            local svc init
            init="${INITSYS}" svc=$(yaml_array "${SERVICES[@]}") \
                yq -P 'with(.;
                        .manager = env(init) |
                        .services = env(svc) )' \
                    -i "$mods"/services-artix.conf
        fi
    fi
}

#}}}
