#!/bin/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_ADMIN_TOPIC_SH:-} ]] || return 0
ARTOOLS_INCLUDE_ADMIN_TOPIC_SH=1

set -e


artixpkg_admin_topic_usage() {
    local -r COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [PKGBASE]...

    OPTIONS
        -a, --add              Add a topic
        -d, --del              Delete a topic
        -j, --jobs N           Run up to N jobs in parallel (default: $(nproc))
        -h, --help             Show this help text

    EXAMPLES
        $ ${COMMAND} --add mytopic libfoo
        $ ${COMMAND} --del mytopic libbar
        $ ${COMMAND} --add mytopic libfoo libbar
        $ ${COMMAND} --del mytopic libfoo libbar
_EOF_
}



artixpkg_admin_topic() {
    if (( $# < 1 )); then
        artixpkg_admin_topic_usage
        exit 0
    fi

    # options
    local pkgbases=()
    local pkgbase

    local ADD_TOPIC
    local DEL_TOPIC
    local ADD=0
    local DEL=0
    local jobs=
    jobs=$(nproc)

    local RUNCMD=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}

    while (( $# )); do
        case $1 in
            -a|--add)
                (( $# <= 1 )) && die "missing argument for %s" "$1"
                ADD_TOPIC="$2"
                ADD=1
                RUNCMD+=" -a ${ADD_TOPIC}"
                shift 2
            ;;
            --add=*)
                ADD_TOPIC="${1#*=}"
                ADD=1
                RUNCMD+=" --add=${ADD_TOPIC}"
                shift
            ;;
            -d|--del)
                (( $# <= 1 )) && die "missing argument for %s" "$1"
                DEL_TOPIC="$2"
                DEL=1
                RUNCMD+=" -d ${DEL_TOPIC}"
                shift 2
            ;;
            --del=*)
                DEL_TOPIC="${1#*=}"
                DEL=1
                RUNCMD+=" --del=${DEL_TOPIC}"
                shift
            ;;
            -h|--help)
                artixpkg_admin_topic_usage
                exit 0
            ;;
            -j|--jobs)
                (( $# <= 1 )) && die "missing argument for %s" "$1"
                jobs=$2
                shift 2
            ;;
            --)
                shift
                break
            ;;
            -*)
                die "invalid argument: %s" "$1"
            ;;
            *)
                break
            ;;
        esac
    done

    pkgbases+=("$@")

    if [[ -n ${GIT_TOKEN} ]]; then

        # parallelization
        if [[ ${jobs} != 1 ]] && (( ${#pkgbases[@]} > 1 )); then
            if [[ -n ${BOLD} ]]; then
                export ARTOOLS_COLOR=always
            fi
            if ! parallel --bar --jobs "${jobs}" "${RUNCMD}" ::: "${pkgbases[@]}"; then
                die 'Failed to manage some topic, please check the output'
                exit 1
            fi
            exit 0
        fi

        for pkgbase in "${pkgbases[@]}"; do

            # topics meta
            if (( ADD )); then
                local gitname
                gitname=$(get_compliant_name "${pkgbase}")
                if ! add_topic "${gitname}" "${ADD_TOPIC}"; then
                    warning "failed to add the topic: ${ADD_TOPIC}"
                fi
            fi

            if (( DEL )); then
                local gitname
                gitname=$(get_compliant_name "${pkgbase}")
                if ! remove_topic "${gitname}" "${DEL_TOPIC}"; then
                    warning "failed to delete the topic: ${DEL_TOPIC}"
                fi
            fi

        done

    fi
}
