#!/hint/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_REPO_MOVE_SH:-} ]] || return 0
ARTOOLS_INCLUDE_REPO_MOVE_SH=1

set -e


artixpkg_repo_move_usage() {
    local -r COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [SOURCE_REPO] [DEST_REPO] [PKGBASE]...

    OPTIONS
        -p, --push         Push pkgbase
        -h, --help         Show this help text

    EXAMPLES
        $ ${COMMAND} world-gremlins world libfoo
        $ ${COMMAND} -p world-gremlins world libfoo
_EOF_
}

artixpkg_repo_move() {
    if (( $# < 1 )); then
        artixpkg_repo_move_usage
        exit 0
    fi

    # options
    local pkgbases=()
    local pkgbase

    local PUSH=0

    local DEST
    local SRC

    while (( $# )); do
        case $1 in
            -h|--help)
                artixpkg_repo_move_usage
                exit 0
            ;;
            -p|--push)
                PUSH=1
                shift
            ;;
            -*)
                die "invalid argument: %s" "$1"
            ;;
            *)
                break
            ;;
        esac
    done

   	SRC="$1"
    DEST="$2"
    shift 2

    if ! in_array "${SRC}" "${ARTIX_DB[@]}"; then
        die "${SRC} does not exist!"
    fi
    if ! in_array "${DEST}" "${ARTIX_DB[@]}"; then
        die "${DEST} does not exist!"
    fi

    pkgbases+=("$@")

    for pkgbase in "${pkgbases[@]}"; do

        if [[ -d "${pkgbase}" ]];then

            if [[ ! -d "${pkgbase}/.git" ]]; then
                error "Not a Git repository: ${pkgbase}"
                continue
            fi
            ( cd "${pkgbase}" || return

                if ! has_remote_changes; then

                    if [[ ! -f PKGBUILD ]]; then
                        die "No PKGBUILD found in (%s)" "${pkgbase}"
                    fi

                    local commit_msg src_version # dest_version
                    commit_msg=$(get_commit_msg 'move' "${DEST}" "${SRC}")

                    src_version=$(version_from_yaml "${SRC}")
#                     dest_version=$(version_from_yaml "${DEST}")

                    if [[ "$src_version" != null ]]; then

#                         local ret
#                         ret=$(vercmp "$src_version" "$dest_version")
#
#                         if (( ret > 0 )); then

                            update_yaml_move "${SRC}" "${DEST}"

                            if [[ -f .SRCINFO ]]; then
                                rm .SRCINFO
                            fi

                            if [[ -n $(git status --porcelain --untracked-files=no) ]]; then

                                stat_busy 'Staging files'
                                for f in $(git ls-files --modified); do
                                    if [[ "$f" == "${REPO_DB}" ]]; then
                                        git add "$f"
                                    fi
                                done
                                stat_done

                                msg 'Commit'
                                git commit -m "${commit_msg}"

                                if (( PUSH )); then
                                    msg "Push (${pkgbase})"
                                    git push origin master
                                fi

                                msg "Querying ${pkgbase} ..."
                                if ! show_db; then
                                    warning "Could not query ${REPO_DB}"
                                fi

                            fi

#                         elif (( ret < 0 )); then
#
#                             error "${pkgbase}: invalid move: version $src_version < $dest_version!"
#
#                         else
#                             error "${pkgbase}: invalid move: version $src_version = $dest_version!"
#
#                         fi

                    else
                        error "${pkgbase}: invalid move: version $src_version!"
                    fi

                fi

            )
        fi

    done
}
