#!/bin/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_GIT_CREATE_SH:-} ]] || return 0
ARTOOLS_INCLUDE_GIT_CREATE_SH=1

# shellcheck source=src/lib/pkg/git/clone.sh
source "${LIBDIR}"/pkg/git/clone.sh
# shellcheck source=src/lib/pkg/git/config.sh
source "${LIBDIR}"/pkg/git/config.sh

set -e


artixpkg_git_create_usage() {
    local -r COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [PKGBASE]...

    OPTIONS
        -c, --clone           Clone the Git repository after creation
        -t, --team=NAME       Assign team name (default: world)
                              Possible values: [system, world, lib32, galaxy]
        -h, --help            Show this help text

    EXAMPLES
        $ ${COMMAND} libfoo
_EOF_
}

artixpkg_git_create() {
    # options
    local pkgbases=()
    local pkgbase
    local clone=0
    local config=0
    local TEAM="${ARTIX_DB[5]}"
    local AGENT=()

    local TEAMS=(
        "${ARTIX_DB[2]}"
        "${ARTIX_DB[5]}"
        "${ARTIX_DB[8]}"
        "${ARTIX_DB[11]}"
        )
    # variables
    local path

    while (( $# )); do
        case $1 in
        -h|--help)
            artixpkg_git_create_usage
            exit 0
        ;;
        -c|--clone)
            clone=1
            shift
        ;;
        -t|--team)
            (( $# <= 1 )) && die "missing argument for %s" "$1"
            TEAM="$2"
            shift 2
        ;;
        --team=*)
            TEAM="${1#*=}"
            shift
        ;;
        -*)
            die "invalid argument: %s" "$1"
        ;;
        *)
            pkgbases=("$@")
            break
        ;;
        esac
    done

    if ! in_array "${TEAM}" "${TEAMS[@]}"; then
        die "${TEAM} does not exist!"
    fi

    # check if invoked without any path from within a packaging repo
    if (( ${#pkgbases[@]} == 0 )); then
        if [[ -f PKGBUILD ]]; then
            if ! path=$(realpath -e .); then
                die "failed to read path from current directory"
            fi
        pkgbases=("$(basename "${path}")")
        clone=0
        config=1
    else
        artixpkg_git_create_usage
        exit 1
        fi
    fi

    # create
    for pkgbase in "${pkgbases[@]}"; do
        local gitname
        gitname=$(get_compliant_name "${pkgbase}")
        if [[ -n ${GIT_TOKEN} ]]; then
            if ! create_repo "${gitname}" >/dev/null; then
                die "failed to create project: ${pkgbase}"
            fi
            if ! add_team_to_repo "${gitname}" "${TEAM}" >/dev/null; then
                warning "failed to assign team: ${TEAM}"
            fi
            msg_success "Successfully created ${pkgbase}"
        fi
        if [[ ${TEAM} == "${ARTIX_DB[11]}" ]]; then
            AGENT+=(--agent="${TEAM}")
        fi
        if (( clone )); then
            artixpkg_git_clone "${AGENT[@]}" "${pkgbase}"
        elif (( config )); then
            artixpkg_git_config "${AGENT[@]}"
        fi

    done

    # some convenience hints if not in auto clone/config mode
    if (( ! clone )) && (( ! config )); then
        cat <<- _EOF_

        For new clones:
        $(msg2 "artixpkg git clone ${pkgbases[*]}")
        For existing clones:
        $(msg2 "artixpkg git config ${pkgbases[*]}")
_EOF_
    fi
}
